#include <iostream>

#include "QuickMath.hpp"

int main() {
	using Simple::IsEven;

	if (IsEven(2)) {
		std::cout << "Well, 2 SHOULD be even...but can we test without doing things like this?" << std::endl;
	}
	else {
		std::cout << "The fact that we're here means this code is completely botched." << std::endl;
	}

	return 0;
}