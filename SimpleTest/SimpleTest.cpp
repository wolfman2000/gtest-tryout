#include "QuickMath.hpp"

#include <gtest/gtest.h>

class SimpleTest: public ::testing::Test {
public:
	SimpleTest() {}
};

TEST_F(SimpleTest, IsPositiveTwoEven) {
	ASSERT_EQ(true, Simple::IsEven(2));
}

TEST_F(SimpleTest, IsPositiveOneEven) {
	ASSERT_EQ(false, Simple::IsEven(1));
}

TEST_F(SimpleTest, IsZeroEven) {
	ASSERT_EQ(true, Simple::IsEven(0));
}