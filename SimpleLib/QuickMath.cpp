#include "QuickMath.hpp"

namespace Simple {
	bool IsEven(int const n) {
		return n % 2 == 0;
	}
}